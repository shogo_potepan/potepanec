require 'rails_helper'

RSpec.describe "Potepan::Categories Controller", type: :request do
  let(:taxonomy) { create(:taxonomy) }
  let(:bag) { create(:taxon, name: "Bags", taxonomy: taxonomy, parent: taxonomy.root) }
  let!(:product) { create(:product, name: "Ruby on rails tote", taxons: [bag]) }

  before do
    image = File.open(File.expand_path('../../fixtures/bag.jpg', __FILE__))
    product.images.create(attachment: image)
    get potepan_category_path(bag.id)
  end

  it "正しく接続できること" do
    expect(response).to have_http_status "200"
  end

  it "showページに正しくtaxonomyが取得できていること" do
    expect(response.body).to include product.name
  end

  it "showページに正しくtaxonが取得できていること" do
    expect(response.body).to include product.display_price.to_s
  end
end
