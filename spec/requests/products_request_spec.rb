require 'rails_helper'

RSpec.describe "Products", type: :request do
  describe '#show' do
    let(:taxonomy) { create(:taxonomy, name: "Category") }
    let(:taxon) { create(:taxon, taxonomy: taxonomy, parent: taxonomy.root) }
    let!(:product) { create(:product, name: "Product", price: "19.99", taxons: [taxon]) }
    let!(:related_product) { create(:product, name: "Related_product", price: "13.99", taxons: [taxon]) }
    let(:other_taxonomy) { create(:taxonomy) }
    let(:other_taxon) { create(:taxon, taxonomy: other_taxonomy) }
    let!(:other_category_product) { create(:product, name: "Other_category_product", price: "39.99", taxons: [other_taxon]) }

    before do
      get potepan_product_path(product.id)
      image = File.open(File.expand_path('../../fixtures/bag.jpg', __FILE__))
      related_product.images.create(attachment: image)
      other_category_product.images.create(attachment: image)
    end

    it '正しく接続できること' do
      expect(response).to have_http_status "200"
    end

    it "showページにproduct.nameが含まれていること" do
      expect(response.body).to include product.name
    end

    it "showページにproduct.display_priceが含まれていること" do
      expect(response.body).to include product.display_price.to_s
    end

    it "showページにproduct.descriptionが含まれていること" do
      expect(response.body).to include product.description
    end

    context "現ページの商品と同カテゴリーに属している商品の場合" do
      it "showページにrelated_product.nameが含まれていること" do
        expect(response.body).to include related_product.name
      end

      it "showページにrelated_product.display_priceが含まれていること" do
        expect(response.body).to include related_product.display_price.to_s
      end

      it "showページにrelated_product.descriptionが含まれていること" do
        expect(response.body).to include related_product.description
      end
    end

    context "現ページの商品と異なるカテゴリーに属している商品の場合" do
      it "showページにother_category_product.nameが含まれていないこと" do
        expect(response.body).not_to include other_category_product.name
      end

      it "showページにother_category_product.display_priceが含まれていないこと" do
        expect(response.body).not_to include other_category_product.display_price.to_s
      end
    end

    context "関連商品が5つある場合" do
      let(:bag) { create(:taxon, name: "Bag") }
      let(:ruby) { create(:taxon, name: "Ruby") }
      let!(:ruby_bag) { create(:product, name: "Ruby_bag", taxons: [bag, ruby]) }
      let!(:ruby_mug) { create(:product, name: "Ruby_tote", taxons: [ruby]) }
      let!(:rails_shirts) { create(:product, name: "Rails_shirts", taxons: [bag]) }
      let!(:rails_tote) { create(:product, name: "Rails_tote", taxons: [bag]) }
      let!(:ruby_tote) { create(:product, name: "Ruby_tote", taxons: [ruby]) }
      let!(:ruby_stein) { create(:product, name: "Ruby_stein", taxons: [ruby]) }

      before do
        get potepan_product_path(ruby_bag.id)
      end

      it "関連商品は４つまでしか表示されないこと" do
        expect(controller.instance_variable_get("@related_products").length).to eq 4
      end
    end
  end
end
