require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "Application Title helpers" do
    context "BASE_TITLEが空白の場合" do
      it "BIGBAG Storeが表示されること" do
        expect(full_title("")).to eq "BIGBAG Store"
      end
    end

    context "BASE_TITLEに値がある場合" do
      it "BASE_TITLE | BIGBAG Storeが表示されること" do
        expect(full_title("Help")).to eq "Help | BIGBAG Store"
      end
    end

    context "BASE_TITLEがnilの場合" do
      it "BIGBAG Storeが表示されること" do
        expect(full_title(nil)).to eq "BIGBAG Store"
      end
    end
  end
end
