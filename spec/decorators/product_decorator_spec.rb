require 'rails_helper'

RSpec.describe Potepan::ProductDecorator, type: :model do
  let(:bag) { create(:taxon, name: "Bag") }
  let(:shirts) { create(:taxon, name: "Shirts") }
  let(:ruby) { create(:taxon, name: "Ruby") }
  let(:rails) { create(:taxon, name: "Rails") }
  let!(:ruby_bag) { create(:product, name: "Ruby_bag", taxons: [bag, ruby]) }
  let!(:ruby_mug) { create(:product, name: "Ruby_tote", taxons: [ruby]) }
  let!(:rails_shirts) { create(:product, name: "Rails_shirts", taxons: [rails, shirts]) }
  let!(:rails_tote) { create(:product, name: "Rails_tote", taxons: [rails, bag]) }

  it '同カテゴリーの商品が含まれること' do
    expect(ruby_bag.related_products).to match_array [ruby_mug, rails_tote]
  end

  it "重複レコードは返さないこと" do
    expect(ruby_bag.related_products).not_to include ruby_bag.name
  end
end
