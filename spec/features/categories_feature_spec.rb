require 'rails_helper'

RSpec.feature "Show page test", type: :feature do
  let(:taxonomy) { create(:taxonomy, name: "Category") }
  let(:taxon) { create(:taxon, taxonomy: taxonomy, parent: taxonomy.root) }
  let(:bag) { create(:taxon, name: "Bags", taxonomy: taxonomy, parent: taxon) }
  let(:mug) { create(:taxon, name: "Mugs", taxonomy: taxonomy, parent: taxon) }
  let!(:product_1) { create(:product, name: "Ruby on Rails Tote", price: "15.99", taxons: [bag]) }
  let!(:product_2) { create(:product, name: "Ruby on Rails Bag", price: "16.99", taxons: [bag]) }
  let!(:product_3) { create(:product, name: "Ruby on Rails Mug", price: "13.99", taxons: [mug]) }

  background do
    image = File.open(File.expand_path('../../fixtures/bag.jpg', __FILE__))
    product_1.images.create(attachment: image)
    product_2.images.create(attachment: image)
    product_3.images.create(attachment: image)
    visit potepan_category_path(bag.id)
  end

  scenario "サイドバーの挙動を確認すること" do
    click_on bag.name
    expect(current_path).to eq potepan_category_path(bag.id)
    within '.side-nav' do
      expect(page).to have_content taxonomy.name
      expect(page).to have_content bag.name
      expect(page).to have_content mug.name
    end
  end

  scenario "showページに正常にパスすること" do
    expect(current_path).to eq potepan_category_path(bag.id)
  end

  scenario "リンクが正しいこと" do
    expect(page).to have_link bag.name, href: potepan_category_path(bag.id)
  end

  scenario "同じカテゴリー商品が表示されること" do
    expect(current_path).to eq potepan_category_path(bag.id)
    expect(page).to have_content product_1.name
    expect(page).to have_content product_2.name
    expect(page).not_to have_content product_3.name
  end
end
