require 'rails_helper'

RSpec.feature "Show page test", type: :feature do
  let(:taxonomy) { create(:taxonomy, name: "Category") }
  let(:taxon) { create(:taxon, taxonomy: taxonomy, parent: taxonomy.root) }
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:related_product) { create(:product, name: "Related_product", price: "13.99", taxons: [taxon]) }

  background do
    visit potepan_product_path(product.id)
    image = File.open(File.expand_path('../../fixtures/bag.jpg', __FILE__))
    related_product.images.create(attachment: image)
  end

  scenario "Showページにアクセスすること" do
    within('.media-body') do
      expect(page).to have_title product.name
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      expect(page).to have_content product.description
    end
  end

  scenario "リンクの確認" do
    expect(page).to have_link 'Home', href: potepan_index_path
    expect(page).to have_link '一覧ページへ戻る', href: potepan_category_path(product.taxons.first.id)
  end

  scenario "関連商品のリンクが正しいこと" do
    within('.productsContent') do
      expect(page).to have_link related_product.name, href: potepan_product_path(related_product.id)
      expect(page).to have_link related_product.display_price.to_s, href: potepan_product_path(related_product.id)
    end
  end
end
